package com.builder.task;

import com.builder.InjectTools;
import com.builder.config.BuilderConfig;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.IOException;


public class InjectTask extends DefaultTask {


    private void log(String log) {
        System.out.println("****************" + log + "****************");
    }

    public InjectTask() {
        setGroup(BuilderConfig.GROUP_NAME);
    }

    @TaskAction
    public void task() {
        log("开始执行");
        System.out.println("Gradle 路径:" + getProject().getProjectDir().toString());
        log("开始注入");
        String path = getProject().getParent().getProjectDir().toString();
        BuilderConfig.getInstance().setConfig(path);
        File file = new File("/Users/songdehuai/Desktop/files/a111/info.json");
        try {
            boolean result = InjectTools.getInstance().inject(path, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        log("注入完成");
        log("执行完成");
    }

}
