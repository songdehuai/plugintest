package com.builder.task;

import com.builder.BuilderTools;
import com.builder.FlavorTools;
import com.builder.InjectTools;
import com.builder.config.BuilderConfig;
import com.builder.entity.BuildConfigField;
import com.builder.entity.Flavor;
import com.builder.entity.ResValue;

import org.apache.commons.io.FileUtils;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.IOException;


public class BuilderTask extends DefaultTask {

    private void log(String log) {
        System.out.println("****************" + log + "****************");
    }

    public BuilderTask() {
        setGroup(BuilderConfig.GROUP_NAME);
    }


    @TaskAction
    public void task() {
        log("开始Build");
        String filePath = getProject().getProjectDir().toString();
        try {
            String json = FileUtils.readFileToString(new File("/Users/songdehuai/Desktop/files/a111/info.json"), "UTF-8");
            InjectTools.getInstance().inject(filePath, Flavor.toFlavor(json));
        } catch (IOException e) {
            e.printStackTrace();
        }
        log("BUILD SUCCESS");
    }
}

