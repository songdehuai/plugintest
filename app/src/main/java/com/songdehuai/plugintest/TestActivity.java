package com.songdehuai.plugintest;

import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources res = getResources();

        String layoutName = "activity_main";

        int layoutId = res.getIdentifier(layoutName, "layout", getPackageName());

        setContentView(layoutId);

        TextView textView = findViewById(R.id.test_tv);

        textView.setText(BuildConfig.buildText);


    }

}
