package com.builder.task;

import com.builder.config.BuilderConfig;
import com.builder.utils.ProcessUtils;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

public class CleanAndBuildAll extends DefaultTask {


    public CleanAndBuildAll() {
        setGroup(BuilderConfig.GROUP_NAME);
    }

    @TaskAction
    public void task() {
        //执行打包全部
        ProcessUtils.run("gradle clean assembleRelease");
    }
}
