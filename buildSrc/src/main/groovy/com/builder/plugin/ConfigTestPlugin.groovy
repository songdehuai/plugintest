package com.builder.plugin

import com.android.build.api.transform.QualifiedContent
import com.android.build.api.transform.Transform
import com.android.build.api.transform.TransformException
import com.android.build.api.transform.TransformInvocation
import com.android.build.gradle.internal.dsl.ProductFlavor
import com.android.build.gradle.internal.pipeline.TransformManager
import com.android.builder.model.ClassField
import com.builder.FlavorTools
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Plugin
import org.gradle.api.Project


class ConfigTestPlugin extends Transform implements Plugin<Project> {

    Project mProject
    def android

    @Override
    void transform(TransformInvocation transformInvocation) throws TransformException, InterruptedException, IOException {
        FlavorTools.instance.buildApk("projectTest2")
    }


    @Override
    void apply(Project project) {
        mProject = project

    }


    void getFlavors() {
        NamedDomainObjectContainer<ProductFlavor> flavors = android.getProductFlavors()
        ProductFlavor flavor
        for (int i = 0; i < flavors.size(); i++) {
            flavor = flavors[i]
            System.out.println("************" + flavor.getName() + "**********")
            Map<String, ClassField> fieldMap = flavor.getBuildConfigFields()

            for (Map.Entry<String, ClassField> entry : fieldMap.entrySet()) {
                String mapKey = entry.getKey()
                ClassField mapValue = entry.getValue()
                System.out.println(mapKey + ":" + mapValue.getName())
            }
        }
    }


    @Override
    String getName() {
        return "BuilderPlugin.groovy"
    }

    @Override
    Set<QualifiedContent.ContentType> getInputTypes() {
        return TransformManager.CONTENT_CLASS
    }

    @Override
    Set<? super QualifiedContent.Scope> getScopes() {
        return TransformManager.SCOPE_FULL_PROJECT
    }

    @Override
    boolean isIncremental() {
        return false
    }
}