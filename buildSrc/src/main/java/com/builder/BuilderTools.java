package com.builder;

import com.builder.config.BuilderConfig;
import com.builder.utils.ProcessUtils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * 编译工具类
 *
 * @author songdehuai
 */
public class BuilderTools {

    private static BuilderTools builderTools = new BuilderTools();

    private BuilderTools() {

    }

    public static BuilderTools getInstance() {
        return builderTools;
    }

    public synchronized void buildApk(String projectPath, String flavorName) {
        buildApk(projectPath, flavorName, false, false);
    }

    public synchronized void buildApkWithClean(String projectPath, String flavorName) {
        buildApk(projectPath, flavorName, false, true);
    }

    public synchronized void buildApkRelease(String projectPath, String flavorName) {
        buildApk(projectPath, flavorName, true, true);
    }

    public synchronized void buildApk(String projectPath, String flavorName, Boolean isRelease, boolean isClean) {
        String buildType = "Release";
        String cleanFlag = "clean";
        if (isRelease) {
            buildType = "Release";
        } else {
            buildType = "Debug";
        }
        if (isClean) {
            cleanFlag = "clean";
        } else {
            cleanFlag = "";
        }
        String shFileName = BuilderConfig.TEMP_FILE + "builder.sh";
        String flavorLastStr = flavorName.substring(0, 1).toUpperCase();
        String name = flavorName.replaceFirst(flavorLastStr.toLowerCase(), flavorLastStr);
        File shFile = new File(shFileName);
        String shStr =
                "cd " + projectPath + " && gradle " + cleanFlag + " assemble" + name + buildType + " -q";
        try {
            FileUtils.writeStringToFile(shFile, shStr, BuilderConfig.DEFAULT_CHARSET_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ProcessUtils.Result r = ProcessUtils.run("sh " + shFileName);
        System.out.println("code:" + r.code + "\ndata:" + r.data);
    }


}
